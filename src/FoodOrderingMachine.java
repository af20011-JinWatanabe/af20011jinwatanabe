import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton yakitoriButton;
    private JButton tamagoYakiButton;
    private JButton potatoButton;
    private JTextPane receivedInfo;
    private JButton checkOutButton;
    private JPanel foodMenu;
    private JTabbedPane menuTab;
    private JButton beerButton;
    private JButton highballButton;
    private JButton lemonSourButton;
    private JButton cokeButton;
    private JButton gingerAleButton;
    private JButton orangeJuiceButton;
    private JButton waterButton;
    private JButton clerkCallButton;
    private JButton smallPlateButton;
    private JLabel yenLabel;
    private JLabel totalLabel;
    private JPanel drinkMenu;
    private JPanel servicesMenu;

    public static int total = 0; //合計金額の変数
    public static int flagClerk = 0; //スタッフ呼び出しのフラグ
    public static int orderedMenu = 0; //注文品数

    void order(String itemName, int itemValue){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + itemName + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0){
            orderedMenu++;
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + itemName + "! It will be served as soon as possible");
            String currentText = receivedInfo.getText();
            receivedInfo.setText(currentText + itemName + " " + itemValue + "yen\n");
            total += itemValue;
            yenLabel.setText(total + "yen");
        }
    }

    int warn(){
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Are you over 20?",
                "Age confirmation",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE
        );
        if(confirmation==1){
            JOptionPane.showMessageDialog(null, "The drinking age is 20.", "Age Denied", JOptionPane.WARNING_MESSAGE);
        }
        return confirmation;
    }

    void checkout(int ans){
        int []coins = {5000, 1000, 500, 100, 50, 10, 5, 1};
        int []howManyCoins = new int[8];
        int change = ans - total;

        if(change == 0){
            JOptionPane.showMessageDialog(null, "Thank you for the exact amount.");
        }
        else{
            for (int i = 0; i < 8; i++) {
                howManyCoins[i] = change / coins[i];
                change %= coins[i];
            }
            JOptionPane.showMessageDialog(null, "Here's your change\n5000yen : " + howManyCoins[0] + "\n1000yen : " + howManyCoins[1] + "\n500yen : " + howManyCoins[2] + "\n100yen : " + howManyCoins[3] + "\n50yen : " + howManyCoins[4] + "\n10yen : " + howManyCoins[5] + "\n5yen : " + howManyCoins[6] + "\n1yen : " + howManyCoins[7] + "\nThank you.");
        }
    }

    void initialize(){
        total = 0; //合計金額の変数
        flagClerk = 0; //スタッフ呼び出しのフラグ
        orderedMenu=0; //注文品数
        receivedInfo.setText("");
        yenLabel.setText(total + "yen");
    }

    public FoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 110);
            }
        });
        tempuraButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconTempura.jpg")));

        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage", 90);
            }
        });
        karaageButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconKaraage.jpg")));

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 100);
            }
        });
        gyozaButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconGyoza.jpg")));

        yakitoriButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakitori", 80);
            }
        });
        yakitoriButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconYakitori.jpg")));

        tamagoYakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("TamagoYaki", 85);
            }
        });
        tamagoYakiButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconTamagoYaki.jpg")));

        potatoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Potato", 95);
            }
        });
        potatoButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconPotato.jpg")));

        beerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(warn()==0) order("Beer", 500);
            }
        });
        beerButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconBeer.jpeg")));
        beerButton.setBackground(Color.RED);

        highballButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(warn()==0) order("Highball", 300);
            }
        });
        highballButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconHighball.jpg")));
        highballButton.setBackground(Color.RED);

        lemonSourButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(warn()==0) order("Lemon sour", 400);
            }
        });
        lemonSourButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconLemonsour.jpg")));
        lemonSourButton.setBackground(Color.RED);

        cokeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coke", 250);
            }
        });
        cokeButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconCoke.jpeg")));

        gingerAleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ginger ale", 260);
            }
        });
        gingerAleButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconGingerale.jpeg")));

        orangeJuiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Orange juice", 200);
            }
        });
        orangeJuiceButton.setIcon(new ImageIcon(this.getClass().getResource("itemIcon/iconOrangejuice.jpg")));

        waterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Water", 0);
            }
        });

        smallPlateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Small plate", 0);
            }
        });

        clerkCallButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(flagClerk==0){
                    order("Clerk call", 0);
                    flagClerk = 1;
                }
                else{
                    JOptionPane.showMessageDialog(null, "You have already selected \"Clerk call\"");
                }
            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //なにか注文されていたらチェックアウト
                if(orderedMenu==0){
                    JOptionPane.showMessageDialog(null, "You have not ordered anything");
                }
                else {
                    int confirmation = JOptionPane.showConfirmDialog(
                            null,
                            "Would you like to checkout?",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION
                    );
                    //チェックアウト確定後、もしサービスメニューだけの注文だったらそのまま終了
                    if (confirmation == 0) {
                        if(total==0){
                            JOptionPane.showMessageDialog(null, "Thank you. Please, just a moment.");
                            initialize(); //初期化して終了
                        }
                        else
                        {
                            JOptionPane.showMessageDialog(null, "Thank you. The total price is " + total + " yen.");
                            int ans = 0;
                            boolean err = false;
                            String input;
                            while (true) {
                                do {
                                    input = JOptionPane.showInputDialog(
                                            null,
                                            "Please enter an amount...",
                                            "Check out",
                                            JOptionPane.INFORMATION_MESSAGE
                                    );
                                    if(input == null){
                                        err = true;
                                        break;
                                    }
                                    if (input.matches("^[0-9]+$")) {
                                        ans = Integer.parseInt(input);
                                    } else {
                                        JOptionPane.showMessageDialog(null, "Enter a number please.", "Amount Denied", JOptionPane.WARNING_MESSAGE);
                                    }
                                }while(!input.matches("[0-9]+$"));

                                if(err) {
                                    break;
                                }
                                else if(ans - total < 0) {
                                    JOptionPane.showMessageDialog(null, "The amount is not enough.", "Amount Denied", JOptionPane.WARNING_MESSAGE);
                                }
                                else{
                                    checkout(ans);
                                    break;
                                }
                            }
                            if(err){
                                JOptionPane.showMessageDialog(null, "Amount payment has been canceled", "Canceled Message", JOptionPane.WARNING_MESSAGE);
                            }else{
                                initialize(); //初期化して終了
                            }
                        }
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
